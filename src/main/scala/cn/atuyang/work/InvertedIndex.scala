package cn.atuyang.work

import org.apache.log4j.{Level, Logger}
import org.apache.spark.sql.SparkSession

object InvertedIndex {

  Logger.getLogger("org").setLevel(Level.OFF)

  def main(args: Array[String]): Unit = {

    val ss = SparkSession
      .builder()
      .appName("InvertedIndex")
      .config("spark.sql.shuffle.partitions", 10)
      .master("local[*]")
      .getOrCreate()

    val res = ss.sparkContext.textFile("D:\\intellijWorkSpace\\aTuYang.s\\work.s\\work-bigdata0815\\input\\sample.txt", 10)

    // 打印反向文件索引
    println("打印反向文件索引")
    res.flatMap {
      lines =>
        val line = lines.split("\\.", -1)
        val trimLine = line(1).trim
        trimLine.substring(1, trimLine.length - 1).split(" ", -1).map {
          v =>
            (v, line(0))
        }
    }.distinct().groupByKey()
      .sortBy(_._1,true)
      .foreach(x => println(s"${x._1}: {${x._2.mkString(",")}}"))

    // 打印词频复带的反向文件索引
    println("打印词频复带的反向文件索引")
    res.flatMap {
      lines =>
        val line = lines.split("\\.", -1)
      val trimLine = line(1).trim
        trimLine.substring(1, trimLine.length - 1).split(" ", -1).map {
          v =>
            (v, line(0))
        }
    }.groupByKey()
      .sortBy(_._1,true)
      .foreach(x => println(s"${x._1}: {${x._2.map((_, 1)).groupBy(_._1)
        .mapValues(_.reduceRight((left, right) => (left._1, left._2 + right._2))).values.mkString(", ")}}"))
  }
}
