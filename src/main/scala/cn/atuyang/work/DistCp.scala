package cn.atuyang.work

import com.esotericsoftware.kryo.KryoSerializable
import com.twitter.chill.KryoSerializer
import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.fs.{FileSystem, FileUtil, Path}
import org.apache.log4j.{Level, Logger}
import org.apache.spark.{SparkConf, SparkContext}
import org.apache.spark.sql.SparkSession

import scala.collection.mutable.{ArrayBuffer, ListBuffer}

object DistCp {

  Logger.getLogger("org").setLevel(Level.ERROR)

  def main(args: Array[String]): Unit = {
    val input = args(0)
    val output = args(1)

    val distCpWorker = new DistCpWorker()
    distCpWorker.checkDir(new Path(input), new Path(output))
    distCpWorker.copyFiles()
  }
}

case class DistCpWorker() {

  val conf = new Configuration()
  conf.set("fs.defaultFS", "hdfs://vm.hadoop:9000")
  val fileSystem = FileSystem.get(conf)
  val fileList = new ListBuffer[(Path, Path)]

  /**
   * 创建文件夹
   * @param input
   * @param output
   */
  def checkDir(input: Path, output: Path): Unit = {
    fileSystem.listStatus(input).foreach(x => {
      if (x.isDirectory) {
        println(x.getPath)
        val subPath = x.getPath.toString.split(input.toString)(1)
        val s = "hdfs://vm.hadoop:9000" + output + subPath
        fileSystem.mkdirs(new Path(s))
        val p = new Path(output + subPath)
        checkDir(x.getPath, p)
      } else {
        fileList.append((x.getPath, output))
      }

    })
  }

  /**
   * 拷贝文件
   */
  def copyFiles(): Unit = {

    val sparkConf = new SparkConf()
      .set("spark.sql.shuffle.partitions", "10")
      .set("spark.serializer", "org.apache.spark.serializer.KryoSerializer")
      .setAppName("cpFiles")
      .setMaster("local[*]")
    val sc = new SparkContext(sparkConf)

    val rdd = sc.makeRDD(fileList, 100)

    rdd.mapPartitions(value => {

      // 由于闭包和序列化等原因，没有实现序列化的对象需要在内部声明使用。不能引用外部的非序列化对象。。。。。
      val conf = new Configuration()
      conf.set("fs.defaultFS", "hdfs://vm.hadoop:9000")
      val fileSystem = FileSystem.get(conf)
      var res = List[Boolean]()

      while(value.hasNext){
        val i = value.next()
        val r = FileUtil.copy(fileSystem, i._1, fileSystem, i._2, false, conf)
        if (r) {
          println(i._1 + "传输到" + i._2 + "成功")
        }
        res.::= (r)
      }
      res.toIterator
    }).collect()
  }
}
